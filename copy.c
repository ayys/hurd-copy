/* copy.c - Copy the contents of one file to another
   in a hurdish way.
   */

#define _GNU_SOURCE 1 /* necessary for HURD specific code */

#include <hurd.h>
#include <hurd/io.h>

#include <fcntl.h>
#include <stdio.h>
#include <errno.h>
#include <error.h>

#define BUFLEN 10 /* Arbitrary buffer length used to copy file */

int main(int argc, char *argv[])
{
  file_t in, out;
  mach_msg_type_number_t rd_amount, wr_amount;
  char *buf, *ptr;

  error_t err;

  if (argc != 3)
    error(1, 0, "Usage: %s <input file> <output file>\n", argv[0]);

  /* Create buffer */
  buf = malloc(BUFLEN + 1);
  if (buf == NULL)
    error(1, 0, "Out of memory!");

  /* Open files */
  in = file_name_lookup(argv[1], O_READ, 0);
  if (in == MACH_PORT_NULL)
    error(1, errno, "Could not open file %s", argv[1]);
  out = file_name_lookup(argv[2], O_WRITE|O_CREAT|O_TRUNC, 0640);
  if (out == MACH_PORT_NULL)
    error(1, errno, "Could not open file %s", argv[2]);

  while (1)
  {
    /* Read the first file `in' */
    err = io_read(in, &buf, &rd_amount, -1, BUFLEN);
    if (err)
      error(1, err, "Could not read from file %s", argv[1]);

    if (rd_amount == 0)
      break;
    /* write */
    ptr = buf;
    do
    {
      err = io_write(out, ptr, rd_amount, -1, &wr_amount);
      if (err)
        error(1, err, "Could not write to file %s", argv[2]);
      rd_amount -= wr_amount;
      ptr += wr_amount;
    } while (rd_amount);
  }
  mach_port_deallocate(mach_task_self(), in);
  mach_port_deallocate(mach_task_self(), out);
  return 0;
}
